## crosshatch-user 12 SP1A.210812.016.A2 7979095 release-keys
- Manufacturer: google
- Platform: sdm845
- Codename: crosshatch
- Brand: google
- Flavor: crosshatch-user
- Release Version: 12
- Id: SP1A.210812.016.A2
- Incremental: 7979095
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/crosshatch/crosshatch:12/SP1A.210812.016.A2/7979095:user/release-keys
- OTA version: 
- Branch: crosshatch-user-12-SP1A.210812.016.A2-7979095-release-keys
- Repo: google_crosshatch_dump_19705


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
